const puppeteer = require('puppeteer');

(async () => {
  // Random product to test add to cart process
  let itemURL =
    'https://www.walmart.com/ip/Lenovo-3-11-Celeron-4GB-32GB-Chromebook-11-6-HD-Display-Intel-N4020-Dual-Core-Processor-4GB-RAM-32GB-eMMC-Solid-State-Drive-Chrome-OS-Onyx-Black-82BA/402347782';

  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  // Open the URL for  walmart
  await Promise.all([page.waitForNavigation(), await page.goto(itemURL)]);
  await page.screenshot({ path: '1.png', fullPage: true });

  await Promise.all([
    page.waitForNavigation(),
    await page.click(
      '#add-on-atc-container > div:nth-child(1) > section > div.valign-middle.display-inline-block.prod-product-primary-cta.primaryProductCTA-marker > div.prod-product-cta-add-to-cart.display-inline-block > button'
    ),
  ]);

  await page.screenshot({ path: '2.png', fullPage: true });

  /**
   * Checkout Item
   */
  await Promise.all([
    page.waitForNavigation(),
    await page.click(
      '#cart-root-container-content-skip > div:nth-child(1) > div > div.Cart-PACModal.standard-pac.pac-added.pac-new-ny.no-price-fulfillment.pac-vanilla-hf > div > div > div > div > div.Cart-PACModal-Body-right-rail.Grid-col.u-size-1.u-size-1-2-m.u-size-1-2-l > div > div > div.Grid-col.u-size-1-2.pos-actions-container > div.cart-pos-main-actions.s-margin-top > div.new-ny-styling.cart-pos-proceed-to-checkout > div > button.button.ios-primary-btn-touch-fix.hide-content-max-m.checkoutBtn.button--primary'
    ),
  ]);

  await page.screenshot({ path: '3.png', fullPage: true });

  /**
   * sign in user
   */
  await page.type('#sign-in-email', 'anas.jam89@gmail.com');
  await page.type('.show-hide', 'password');
  await Promise.all([
    page.waitForNavigation(),
    await page.click(
      'body > div.js-content > div > div.checkout-wrapper > div > div.accordion-inner-wrapper > div.checkout-accordion > div > div > div > div.CXO_module_container > div > div > div > div > div.CXO_welcome-mat.ny-lite-wm-variation.borderless > div > div:nth-child(4) > div > section > div > section > form > div.form-actions > button'
    ),
  ]);

  await page.screenshot({ path: '4.png', fullPage: true });

  /**
   * Delivery and PickUp
   */
  await Promise.all([
    page.waitForNavigation(),
    await page.click(
      'body > div.js-content > div > div.checkout-wrapper > div > div.accordion-inner-wrapper > div.checkout-accordion > div > div > div > div:nth-child(2) > div > div.CXO_module_body.ResponsiveContainer > div > div > div > div.text-left.Grid > div > div > div.CXO_fulfillment_continue > button'
    ),
  ]);

  await page.screenshot({ path: '5.png', fullPage: true });

  /**
   * Delivery and PickUp
   */
  // await Promise.all([
  //   page.waitForNavigation(),
  //   await page.click(
  //     'body > div.js-content > div > div.checkout-wrapper > div > div.accordion-inner-wrapper > div.checkout-accordion > div > div > div > div:nth-child(2) > div > div.CXO_module_body.ResponsiveContainer > div > div > div > div.text-left.Grid > div > div > div.CXO_fulfillment_continue > button'
  //   ),
  // ]);

  // await page.screenshot({ path: 'DeliveryPickUp.png', fullPage: true });

  await browser.close();
})();
